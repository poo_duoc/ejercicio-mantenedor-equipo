/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package equiposdefutbol;

import java.util.ArrayList;

/**
 *
 * @author Duoc
 */
public class MantenedorEquipo {
    
    
    private static ArrayList<Equipo> valores = new ArrayList<>();
    
    public static void agregarEquipo(Equipo equipo){
        valores.add(equipo);
    }    
        
    public static boolean eliminarEquipo(String nombre){
        boolean eliminoRegistro = false;
        for(int x = 0; x < valores.size(); x++){
           if(valores.get(x).getNombre().equals(nombre)){
                valores.remove(x);
                eliminoRegistro = true;
                break;
            } 
        }
        return eliminoRegistro;
    }
    
    public static ArrayList<Equipo> listarEquipo(){
        return valores;
    }

    
}
